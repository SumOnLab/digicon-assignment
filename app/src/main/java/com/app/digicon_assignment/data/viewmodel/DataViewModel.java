package com.app.digicon_assignment.data.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.app.digicon_assignment.data.model.User;
import com.app.digicon_assignment.data.repo.DataRepo;

import java.util.List;

public class DataViewModel extends ViewModel {

    private final DataRepo dataRepo;

    public DataViewModel() {
        dataRepo = new DataRepo();
    }

    public LiveData<List<User>> getUsers(int start, int limit){
        return dataRepo.getUsers(start, limit);
    }

}
