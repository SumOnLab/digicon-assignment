package com.app.digicon_assignment.data.repo;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.app.digicon_assignment.data.model.User;
import com.app.digicon_assignment.data.remote.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataRepo {

    private static final String TAG = "DataRepo";

    public MutableLiveData<List<User>> getUsers(int start, int limit) {
        MutableLiveData<List<User>> data = new MutableLiveData<>();
        RetrofitClient.getRetrofitService()
                .getUsers(start, limit)
                .enqueue(new Callback<List<User>>() {
                    @Override
                    public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                        Log.d(TAG, "onResponse: " + response);
                        data.setValue(response.body());
                    }

                    @Override
                    public void onFailure(Call<List<User>> call, Throwable t) {
                        Log.d(TAG, "onFailure: " + t.getMessage());
                        data.setValue(null);
                    }
                });

        return data;
    }
}
