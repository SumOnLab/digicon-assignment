package com.app.digicon_assignment.data.remote;

import com.app.digicon_assignment.data.model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RetrofitService {
    @GET("users")
    Call<List<User>> getUsers(
            @Query("_start") int _start,
            @Query("_limit") int _limit
    );

    @GET("posts")
    Call<List<User>> getPost(
            @Query("_start") int _start,
            @Query("_limit") int _limit
    );
}
