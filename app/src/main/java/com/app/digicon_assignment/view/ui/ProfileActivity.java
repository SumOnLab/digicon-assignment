package com.app.digicon_assignment.view.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.app.digicon_assignment.databinding.ActivityProfileBinding;

public class ProfileActivity extends AppCompatActivity {
    
    private ActivityProfileBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityProfileBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        // new SimpleDateFormat("dd MMMM yyyy", Locale.ENGLISH).format(new Date())

        binding.addPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AddPostDialog().show(getSupportFragmentManager(), AddPostDialog.TAG);
            }
        });

    }
}