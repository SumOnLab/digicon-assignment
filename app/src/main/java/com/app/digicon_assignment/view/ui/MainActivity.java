package com.app.digicon_assignment.view.ui;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.digicon_assignment.data.model.User;
import com.app.digicon_assignment.data.viewmodel.DataViewModel;
import com.app.digicon_assignment.databinding.ActivityMainBinding;
import com.app.digicon_assignment.view.adapter.AdapterUser;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

   /*

    Assignment
    -------------------------------
    make an app using java or kotlin to show a list of user( pagination mandatory),
    where each user may have their profile picture, name , phone or email

    if click on user, user details will be showed
    (make sure age will be calculated from date of birth)
    user also can add post, post may have (picture, title, description)
    when new post come, all user will be notified by push notification
    (optional but will get priority)

    note:( for api, you can use your own api if you have or  you can take help from online like jsonplaceholder)
    deadline : 27/09/2021

    */

    private ActivityMainBinding binding;
    private AdapterUser adapterUser;
    private int POSITION = 0;
    private int LIMIT = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        adapterUser = new AdapterUser(this);

        binding.rvUser.setHasFixedSize(true);
        binding.rvUser.setLayoutManager(new LinearLayoutManager(this));
        binding.rvUser.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        binding.rvUser.setAdapter(adapterUser);

        binding.rvUser.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(RecyclerView.FOCUS_DOWN)
                        && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    Log.d(TAG, "end of list");
                    POSITION = POSITION + LIMIT;
                    loadData();
                }
            }
        });

        loadData();

    }

    private void loadData() {

        binding.progressHorizontal.setVisibility(View.VISIBLE);

        Log.d(TAG, "loadData: from = " + POSITION + " to = " + (POSITION + LIMIT));

        DataViewModel dataViewModel = new DataViewModel();
        dataViewModel.getUsers(POSITION, LIMIT).observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(List<User> users) {

                binding.progressHorizontal.setVisibility(View.GONE);

                Log.d(TAG, "onChanged: " + users);
                if (users.isEmpty()) {
                    Toast.makeText(MainActivity.this, "No item left!!!", Toast.LENGTH_SHORT).show();
                }
                adapterUser.setUser(users);
            }
        });
    }
}